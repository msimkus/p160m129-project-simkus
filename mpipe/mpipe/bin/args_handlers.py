import click


def handle_column_delimiter(ctx, param, value):
    value = "\t" if value == "\\t" else value
    if len(value) > 1:
        msg = ("delimiter must be a 1-character string or \\t,"
               " but given: {}".format(value))
        raise click.BadParameter(msg)
    return value
