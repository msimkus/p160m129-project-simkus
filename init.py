import sys
import subprocess

if __name__ == "__main__":
    if len(sys.argv) != 2 and (sys.argv[1] != 1 or sys.argv[1] != 2):
        print("Error: insufficient or incorrect parameters passed")
        sys.exit(1)

    cmd_main = " ".join([
        "python3",
        "main.py",
        sys.argv[1]
    ])
    subprocess.run(cmd_main, shell=True)
